<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HOME
Route::get('/', 'HomeController@index')->name('home.index');

// NEWS
Route::get('tin-tuc', 'NewsController@index')->name('news.index');
Route::get('tin-tuc/chi-tiet/{slug}.{id}.html', 'NewsController@detail')->name('news.detail');

// SCORECARD
Route::get('xem-diem', 'ScorecardController@index')->name('scorecard.index');

// Attendance
Route::get('diem-danh', 'ScorecardController@index')->name('scorecard.index');



// LOGIN
Route::get('dang-nhap', 'LoginController@getLogin')->name('login.get');
Route::post('dang-nhap', 'LoginController@postLogin')->name('login.post');

// Reset Password
Route::get('khoi-phuc-mat-khau', 'ResetPasswordController@getForm')->name('get.password.reset');
Route::post('khoi-phuc-mat-khau', 'ResetPasswordController@sendCode');

Route::get('mat-khau/cap-nhat', 'ResetPasswordController@resetPassword')->name('update.password');
Route::post('mat-khau/cap-nhat', 'ResetPasswordController@updatePassword')->name('update.password');

// LOGOUT
Route::get('dang-xuat', 'LoginController@logout')->name('logout');

// CONTACT
Route::get('lien-he', 'ContactController@index')->name('contact.index');
Route::post('lien-he', 'ContactController@postContact')->name('contact.post');

// Check đăng tin
Route::get('/cham-diem', function () {
    return view('welcome');
})->name('post.form');


// Tìm kiếm
Route::get('tim-kiem', 'SearchController@search')->name('search');
Route::get('tim-kiem-diem-sv', 'SearchController@homeSearch')->name('home.search');

// Tìm kiếm theo học kì
Route::get('searchSemester', 'SearchController@searchSemester')->name('searchSemester');

