<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'full_name',
        'email',
        'sex',
        'mobile',
        'description'
    ];

    protected $filter = [
        'id',
        'full_name',
        'email',
        'sex',
        'mobile',
        'description'
    ];
}
