<?php


namespace App\Presenters;

use App\Presenters\Traits\TimestampsTrait;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use McCool\LaravelAutoPresenter\BasePresenter;

class NewsPresenter extends BasePresenter implements Arrayable
{
    use TimestampsTrait;

    public function getCategoryString($d)
    {
        if (! empty($d)) {
            $type = [
                1 => 'Tin tức',
                2 => 'Thông báo',

            ];

            return $type[$d];
        }

        return 'Không xác định';
    }

    public function getNewsCategoryString()
    {
        return $this->getCategoryString($this->wrappedObject->category_id);
    }

    /**
     * Convert the presenter instance to an array.
     *
     * @return string[]
     */
    public function toArray()
    {
        return array_merge($this->wrappedObject->toArray(), [
            'news_category_string'          => $this->getNewsCategoryString(),
            'pending_delete'                => $this->wrappedObject->pending_delete ? $this->utc_to_hcm($this->wrappedObject->pending_delete) : null,
            'created_at'                    => $this->created_at(),
            'updated_at'                    => $this->updated_at(),
        ]);
    }
}