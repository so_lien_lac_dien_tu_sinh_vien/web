<?php


namespace App\Presenters\Traits;

use Carbon\Carbon;

trait TimestampsTrait
{
    /**
     * Present formatted date time.
     *
     * @return string
     */
    public function created_at()
    {
        return $this->utc_to_hcm($this->wrappedObject->created_at);
    }

    /**
     * Present formatted date time.
     *
     * @return string
     */
    public function updated_at()
    {
        return $this->utc_to_hcm($this->wrappedObject->updated_at);
    }

    public function utc_to_hcm($timestamp)
    {
        if (! $timestamp) {
            return '';
        }

        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, 'UTC')
            ->setTimezone('Asia/Ho_Chi_Minh')
            ->format('H:i:s d/m/Y');
    }
}