<?php

namespace App;

use App\Presenters\NewsPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class News extends Model implements HasPresenter
{
    //
    public function getPresenterClass()
    {
        return NewsPresenter::class;
    }
}
