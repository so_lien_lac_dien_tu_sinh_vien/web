<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Classification;
use App\TrainingPoint;
use App\SchoolYear;
use App\Subject;
use App\Scorecard;
use Illuminate\Http\Request;
use Auth;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $request->validate(
            [
                'query' => 'required|min:3',
            ],
            [
                'query.required' => 'Vui lòng nhập ký tự để tìm kiếm!',
                'query.min' => 'Vui lòng nhập ít nhất 3 kí tự!',
            ]
        );

        // ĐIỂM SỐ =========================================================
         $stud = Auth::user()->code_number_student;

//         dd($stud);
         $checkYear = Scorecard::query()->where('code_number_student', $stud)->get();

        // dd($checkYear);
         // Start tích lũy
         $getTichLuy = [];
         // $a = 0;
         $valTL = 0;
         foreach ($checkYear as $getEx) {
            // $a++;
            $monhoc = Subject::where('code_number_subject', '=', $getEx['code_number_subject'])->first();
            // dd($getEx['code_number_subject']);
            // dd($monhoc['count_credit']);
            array_push($getTichLuy, [
                    'tichluy'  => $getEx['medium_score'] * $monhoc['count_credit'],
                    'sotinchi' => $monhoc['count_credit']
                ]);
         }
         // dd($monhoc);
         // dd($getTichLuy);
         $tongtinchi = 0;
         foreach ($getTichLuy as $val) {
             $valTL += $val['tichluy'];
             $tongtinchi +=  $val['sotinchi'] ;
             // dd($valTL);
         }
         // dd($valTL);
         // dd($tongtinchi);
         $sumDiemTichLuy = $valTL/($tongtinchi);
         // dd($sumDiemTichLuy);
         // End tích lũy

        $grouped = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0);
        });

//        dd($grouped);

        $SchoolYear1 = $grouped->map(function ($item, $key) {
            return $key;
        });

//        dd($SchoolYear1);

         $SchoolYear = [];
        foreach($SchoolYear1 as $item) {
                // dd($item);

              $SchoolYearcheck = SchoolYear::where('id', $item)->get();

                array_push($SchoolYear, [
                    'id'  => $SchoolYearcheck,
                ]);
        }


        $groupedByID = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0, 4);
        });
        $SchoolYearByID = $groupedByID->map(function ($item, $key) {
            return $key;
        });

//        dd($SchoolYearByID);


        $result = [];

        // Kết quả mỗi học kì
        foreach ($SchoolYearByID as $item) {
            $scorecard = Scorecard::query()
                ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
                ->where('code_number_student', '=', $request->get('query'))
                ->where('code_number_school_year', '=', $item)
                ->get();

                // dd($scorecard);

            // ĐIỂM DANH =========================================================
            $attendance_2019_2020_hk1_yes = Attendance::query()
                ->where('code_number_student', '=', $request->get('query'))
                ->where('code_number_school_year', '=', $item)
                ->where('absent', '=', 1)
                ->count();

            $attendance_2019_2020_hk1_no = Attendance::query()
                ->where('code_number_student', '=', $request->get('query'))
                ->where('code_number_school_year', '=', $item)
                ->where('absent', '=', 2)
                ->count();

            // XẾP LOẠI =========================================================
            $classification = Classification::query()
                ->with('students', 'classrooms', 'schoolYears')
                ->where('code_number_student', '=', $request->get('query'))
                ->where('code_number_school_year', '=', $item)
                ->get();

                // ĐIỂM RÈN LUYỆN =========================================================
            $trainingPoint = TrainingPoint::query()
                ->where('code_number_student', '=', $request->get('query'))
                ->where('code_number_school_year', '=', $item)
                ->get();


            // XẾP LOẠI =========================================================

            if(count($scorecard) > 0) {
                array_push($result, [
                    'scorecard'  => $scorecard,
                    'attendanceYes'  => $attendance_2019_2020_hk1_yes,
                    'attendanceNo'  => $attendance_2019_2020_hk1_no,
                    'classification'  => $classification,
                    'trainingPoint' => $trainingPoint
                ]);
            }
//
             // dd($result);
        }
        // dd($scorecard);

        return view('scorecard.index', [
            'scorecard' => $scorecard,
            // Điểm danh
            'SchoolYear' => $checkYear,
            'SchoolYearOptions' => $SchoolYear,
            'result' => $result,
            'sumDiemTichLuy' => $sumDiemTichLuy
        ]);
    }

    public function searchSemester(Request $request) {

        $query = $request->input('semester');
        $SchoolYearBySearch = SchoolYear::where('id', $query)->first();


        $stud = Auth::user()->code_number_student;

        $checkYear = Scorecard::where('code_number_student', $stud)->get();



        $grouped = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0, 4);
        });

        $SchoolYear1 = $grouped->map(function ($item, $key) {
            return $key;
        });

        $sumDiemTichLuy = 0;


         $SchoolYear = [];
        foreach($SchoolYear1 as $item) {
                // dd($item);

              $SchoolYearcheck = SchoolYear::where('id', $item)->get();

                array_push($SchoolYear, [
                    'id'  => $SchoolYearcheck,
                ]);

        }


        $groupedByID = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0, 4);
        });
        $SchoolYearByID = $groupedByID->map(function ($item, $key) {
            return collect($item)->count();
        });


        // $SchoolYear = SchoolYear::all();
        $result = [];

        if($query != "null")
        {
            $scorecard = Scorecard::query()
                ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $SchoolYearBySearch->id)
                ->get();


            // ĐIỂM DANH =========================================================
            $attendance_2019_2020_hk1_yes = Attendance::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $SchoolYearBySearch->id)
                ->where('absent', '=', 1)
                ->count();

            $attendance_2019_2020_hk1_no = Attendance::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $SchoolYearBySearch->id)
                ->where('absent', '=', 2)
                ->count();

            // XẾP LOẠI =========================================================
            $classification = Classification::query()
                ->with('students', 'classrooms', 'schoolYears')
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $SchoolYearBySearch->id)
                ->get();

                 // ĐIỂM RÈN LUYỆN =========================================================
            $trainingPoint = TrainingPoint::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item)
                ->get();

            if(count($scorecard) > 0) {
                array_push($result, [
                    'scorecard'  => $scorecard,
                    'attendanceYes'  => $attendance_2019_2020_hk1_yes,
                    'attendanceNo'  => $attendance_2019_2020_hk1_no,
                    'classification'  => $classification,
                    'trainingPoint' => $trainingPoint
                ]);
            }

        } else {
            foreach ($SchoolYear as $item) {
            $scorecard = Scorecard::query()
                ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item->id)
                ->get();

            // ĐIỂM DANH =========================================================
            $attendance_2019_2020_hk1_yes = Attendance::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item->id)
                ->where('absent', '=', 1)
                ->count();

            $attendance_2019_2020_hk1_no = Attendance::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item->id)
                ->where('absent', '=', 2)
                ->count();

            // XẾP LOẠI =========================================================
            $classification = Classification::query()
                ->with('students', 'classrooms', 'schoolYears')
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item->id)
                ->get();

                 // ĐIỂM RÈN LUYỆN =========================================================
            $trainingPoint = TrainingPoint::query()
                ->where('code_number_student', '=', $stud)
                ->where('code_number_school_year', '=', $item)
                ->get();

            if(count($scorecard) > 0) {
                array_push($result, [
                    'scorecard'  => $scorecard,
                    'attendanceYes'  => $attendance_2019_2020_hk1_yes,
                    'attendanceNo'  => $attendance_2019_2020_hk1_no,
                    'classification'  => $classification,
                    'trainingPoint' => $trainingPoint
                    ]);
                }

             // dd($scorecard);
            }
        }

//        session(['query' => $query]);
//        dd(session('query'));

        // SEACH BUTTON

         return view('scorecard.index', [
             'scorecard' => $scorecard,
             // Điểm danh
             'SchoolYear' => $checkYear,
             'SchoolYearOptions' => $SchoolYear,
             'result' => $result,
             'sumDiemTichLuy' => $sumDiemTichLuy
         ]);
    }

    public function homeSearch()
    {
         $stud = Auth::user()->code_number_student;

//         dd($stud);
         $checkYear = Scorecard::query()->where('code_number_student', $stud)->get();

        // dd($checkYear);
         // Start tích lũy
         $getTichLuy = [];
         // $a = 0;
         $valTL = 0;
         foreach ($checkYear as $getEx) {
            // $a++;
            $monhoc = Subject::where('code_number_subject', '=', $getEx['code_number_subject'])->first();
            // dd($getEx['code_number_subject']);
            // dd($monhoc['count_credit']);
            array_push($getTichLuy, [
                    'tichluy'  => $getEx['medium_score'] * $monhoc['count_credit'],
                    'sotinchi' => $monhoc['count_credit']
                ]);
         }
         // dd($monhoc);
         // dd($getTichLuy);
         $tongtinchi = 0;
         foreach ($getTichLuy as $val) {
             $valTL += $val['tichluy'];
             $tongtinchi +=  $val['sotinchi'] ;
             // dd($valTL);
         }
         // dd($valTL);
         // dd($tongtinchi);
         $sumDiemTichLuy = $valTL/($tongtinchi);
         // dd($sumDiemTichLuy);
         // End tích lũy

        $grouped = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0);
        });

//        dd($grouped);

        $SchoolYear1 = $grouped->map(function ($item, $key) {
            return $key;
        });

//        dd($SchoolYear1);

         $SchoolYear = [];
        foreach($SchoolYear1 as $item) {
                // dd($item);

              $SchoolYearcheck = SchoolYear::where('id', $item)->get();

                array_push($SchoolYear, [
                    'id'  => $SchoolYearcheck,
                ]);
        }

        // Check năm học

        // END check năm

        $groupedByID = $checkYear->groupBy(function ($item, $key) {
            return substr($item['code_number_school_year'], 0, 4);
        });
        $SchoolYearByID = $groupedByID->map(function ($item, $key) {
            return $key;
        });

//        dd($SchoolYearByID);


        $result = [];

        // Kết quả mỗi học kì
        foreach ($SchoolYearByID as $item) {
            $scorecard = Scorecard::query()
                ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
                ->where('code_number_student', '=', Auth::user()->code_number_student)
                ->where('code_number_school_year', '=', $item)
                ->get();

                // dd($scorecard);

            // ĐIỂM DANH =========================================================
            $attendance_2019_2020_hk1_yes = Attendance::query()
                ->where('code_number_student', '=', Auth::user()->code_number_student)
                ->where('code_number_school_year', '=', $item)
                ->where('absent', '=', 1)
                ->count();

            $attendance_2019_2020_hk1_no = Attendance::query()
                ->where('code_number_student', '=', Auth::user()->code_number_student)
                ->where('code_number_school_year', '=', $item)
                ->where('absent', '=', 2)
                ->count();

            // XẾP LOẠI =========================================================
            $classification = Classification::query()
                ->with('students', 'classrooms', 'schoolYears')
                ->where('code_number_student', '=', Auth::user()->code_number_student)
                ->where('code_number_school_year', '=', $item)
                ->get();

                // ĐIỂM RÈN LUYỆN =========================================================
            $trainingPoint = TrainingPoint::query()
                ->where('code_number_student', '=', Auth::user()->code_number_student)
                ->where('code_number_school_year', '=', $item)
                ->get();


            // XẾP LOẠI =========================================================

            if(count($scorecard) > 0) {
                array_push($result, [
                    'scorecard'  => $scorecard,
                    'attendanceYes'  => $attendance_2019_2020_hk1_yes,
                    'attendanceNo'  => $attendance_2019_2020_hk1_no,
                    'classification'  => $classification,
                    'trainingPoint' => $trainingPoint
                ]);
            }
//
             // dd($result);
        }
        // dd($scorecard);

        return view('scorecard.index', [
            'scorecard' => $scorecard,
            // Điểm danh
            'SchoolYear' => $checkYear,
            'SchoolYearOptions' => $SchoolYear,
            'result' => $result,
            'sumDiemTichLuy' => $sumDiemTichLuy
        ]);
    }
}
