<?php

namespace App\Http\Controllers;

use App\news;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $news = news::all();
        return view('news.index', [
            'news' => $news
        ]);
    }

    public function detail($slug, $id)
    {
        $news = news::query()->findOrFail($id);
        $similar_data =  News::query()->where('id', '!=' , $id)->take(4)->get();

        return view('news.details', [
            'item' => $news,
            'similar_data' => $similar_data
        ]);
    }
}
