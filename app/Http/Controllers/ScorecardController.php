<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Scorecard;
use Illuminate\Http\Request;

class ScorecardController extends Controller
{
    public function index()
    {
        $scorecard = Scorecard::query()
            ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
            ->where('code_number_student', '=', 'SV001')
            ->where('code_number_school_year', '=', 1)
            ->get();

        // START Demo vắng có/ không phép (1: có phép, 2: không phép)
        $attendance = Attendance::query()
            ->with('students', 'subjects', 'classrooms', 'schoolYears')
            ->where('code_number_student', '=', 'SV001')
            ->where('code_number_school_year', '=', 1)
            ->where('absent', '=', 1)
            ->get();

        $attendance2 = Attendance::query()
            ->with('students', 'subjects', 'classrooms', 'schoolYears')
            ->where('code_number_student', '=', 'SV001')
            ->where('code_number_school_year', '=', 2)
            ->where('absent', '=', 2)
            ->get();

        // END Demo vắng có/ không phép

        $scorecard2 = Scorecard::query()
            ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
            ->where('code_number_student', '=', 'SV001')
            ->where('code_number_school_year', '=', 2)
            ->get();

        $scorecard3 = Scorecard::query()
            ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
            ->where('code_number_student', '=', 'SV001')
            ->where('code_number_school_year', '=', 3)
            ->get();

        return view('scorecard.index', [
            'scorecard' => $scorecard,
            'attendance' => $attendance,
            'attendance2' => $attendance2,
            'scorecard2' => $scorecard2,
            'scorecard3' => $scorecard3
        ]);
    }
}
