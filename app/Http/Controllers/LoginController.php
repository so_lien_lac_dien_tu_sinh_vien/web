<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required',
                'password' => 'required|min:6|max:20',
            ],
            [
                'username.required'         => 'Vui lòng nhập tài khoản!',
                'password.required'         => 'Vui lòng nhập mật khẩu',
                'password.min'              => 'Mật khẩu ít nhất 6 kí tự',
                'password.max'              => 'Mật khẩu không quá 20 kí tự',
            ]
        );

        $credentials = array('username' => $request->username, 'password'=>$request->password, 'role' => 'student');
        if(Auth::attempt($credentials))
        {
            // return redirect()->route('home.index')->with(['flag'=>'success','login-notification'=>'Đăng nhập thành công!']);
             return redirect()->route('home.search')->with(['flag'=>'success','login-notification'=>'Đăng nhập thành công!']);
            // return redirect()->route('search', ['query',Auth::user()->code_number_student]);

        }
        else{
            return redirect()->back()->with(['flag'=>'danger','login-notification'=>'Đăng nhập thất bại! Vui lòng kiểm tra lại tài khoản hoặc mật khẩu?']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home.index');
    }
}
