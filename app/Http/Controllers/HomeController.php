<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $news = news::query()->take(6)->get();
        return view('home.index', [
            'news' => $news
        ]);
    }
}
