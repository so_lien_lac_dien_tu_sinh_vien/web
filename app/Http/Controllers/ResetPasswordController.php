<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResetPasswordCreateRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{
    public function getForm()
    {
        return view('login.reset.sendemail');
    }

    public function sendCode(Request $request)
    {
        $email = $request->email;
        $checkUser = User::query()
            ->with('teachers')
            ->whereHas('teachers', function($teachers) use($email) {
                $teachers->where('email', '=', $email);
            })
            ->first();

        if (!$checkUser) {
            return redirect()->back()->with(['flagMail'=>'danger','mail-notification'=>'Email không tồn tại!']);
        }

        $code = bcrypt(md5(time() . $email));
        $checkUser->code = $code;
        $checkUser->time_code = Carbon::now();
        $checkUser->save();

        // Gửi mail nhận để reset
        $url = route('update.password', ['code' => $checkUser->code, 'email' => $email]);
        $data = [
            'route' => $url
        ];
        Mail::send('login.reset.email', $data, function ($message) use ($email) {
            $message->to($email, 'Reset Password')->subject('Lấy lại mật khẩu');
        });

        return redirect()->back()->with(['flagMail'=>'success','mail-notification'=>'Link lấy lại mật khẩu đã được gửi vào email của bạn!']);
    }

    public function resetPassword(Request $request)
    {
//        $code = $request->code;
//        $email = $request->email;

//        $checkUser = User::query()
//            ->with('teachers')
//            ->where('code', '=', $code)
//            ->whereHas('teachers', function($teachers) use($email) {
//                $teachers->where('email', '=', "%$email%");
//            })
//            ->first();

//        if (!$checkUser) {
//            return redirect()->back()->with(['changePass'=>'danger','password-notification'=>'Xin lỗi! Đường dẫn lấy lại mật khẩu không đúng, bạn vui lòng thử lại sau!']);
//        }

        return view('login.reset.resetpass');
    }

    public function updatePassword(ResetPasswordCreateRequest $requestResetPassword)
    {

        $code = $requestResetPassword->code;
        $email = $requestResetPassword->email;

//        $checkUser = User::query()
//            ->with('teachers')
//            ->where('code', '=', $code)
//            ->whereHas('teachers', function($teachers) use($email) {
//                $teachers->where('email', '=', "%$email%");
//            })
//            ->first();
        $checkUser = User::where(['code' => $code])->first();

        if (!$checkUser) {
            return redirect()->back()->with(['changePass'=>'danger','password-notification'=>'Xin lỗi! Đường dẫn lấy lại mật khẩu không đúng, bạn vui lòng thử lại sau!']);
        }

        if(empty($requestResetPassword->re_password_reset))
        {
            return redirect()->back()->with(['changePass'=>'danger','password-notification'=>'Vui lòng kiểm tra lại thông tin!']);
        }
        $checkUser->password = Hash::make($requestResetPassword->password_reset);
        $checkUser->save();

        return redirect()->route('login.get')->with(['changePass'=>'success','password-notification'=>'Mật khẩu đã được thay đổi thành công!']);
    }
}