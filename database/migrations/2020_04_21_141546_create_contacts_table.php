<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('full_name')->comment('Họ tên');
            $table->string('email')->unique()->comment('Địa chỉ email');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->string('mobile')->unique()->comment('Số điện thoại di động');

            $table->longText('description')->comment('Nôi dung');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
