<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScorecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecards', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_class')->default('')->comment('Lớp học');

            $table->unsignedBigInteger('code_number_school_year')->comment('Năm học');
            $table->string('code_number_subject')->default('')->comment('Môn học');

            $table->string('code_number_student')->default('')->comment('Sinh viên');
            $table->string('code_number_teacher')->default('')->comment('Giảng viên');

            $table->decimal('mid_score')->default(0)->comment('Điểm giữa kì');
            $table->decimal('last_score')->default(0)->comment('Điểm cuối kì');

            $table->decimal('medium_score')->default(0)->comment('Điểm trung bình');

            $table->foreign('code_number_school_year')
                ->references('id')->on('school_years')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_student')
                ->references('code_number_student')->on('students')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_teacher')
                ->references('code_number_teacher')->on('teachers')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_subject')
                ->references('code_number_subject')->on('subjects')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scorecards');
    }
}
