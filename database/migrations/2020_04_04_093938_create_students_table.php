<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();

            // Thông tin cá nhân
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');

            $table->string('code_number_student')->unique()->comment('Mã sinh viên');
            $table->string('last_name')->comment('Họ');
            $table->string('first_name')->comment('Tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->date('birthday')->comment('Ngày sinh');
            $table->string('thumbnails')->default('')->comment('Hình đại diện, đường dẫn trên CDN, không kèm tên miền');

            // Liên hệ và xác minh
            $table->string('email')->unique()->comment('Địa chỉ email');
            $table->timestamp('email_verified_at')->nullable()->comment('Email xác minh thành công khi nào');
            $table->string('identity_number')->default('')->comment('CMND/Thẻ căn cước');

            $table->string('phone')->default('')->comment('Số điện thoại cố định');
            $table->string('mobile')->unique()->comment('Số điện thoại di động');
            $table->timestamp('mobile_verified_at')->nullable()->comment('Số điện thoại xác minh thành công khi nào');

            // Thông tin bổ sung, bổ trợ tìm kiếm

            $table->string('place_of_birth')->default('')->comment('Địa chỉ nơi sinh');
            $table->string('home_town')->default('')->comment('Quê quán');
            $table->string('permanent_residence')->default('')->comment('Hộ khẩu thường trú');
            $table->string('current_home')->default('')->comment('Nơi ở hiện tại');

            $table->string('nation')->default('')->comment('Dân tộc');
            $table->string('religion')->default('')->comment('Tôn giáo');
            $table->string('nationality')->default('')->comment('Quốc tịch');

            $table->string('full_name_father')->default('')->comment('Họ tên cha');
            $table->date('birthday_father')->comment('Năm sinh của cha');
            $table->string('mobile_father')->unique()->comment('Số điện thoại di động của cha');
            $table->string('work_unit_father')->default('')->comment('Đơn vị công tác của cha');

            $table->string('full_name_mother')->default('')->comment('Họ tên mẹ');
            $table->date('birthday_mother')->comment('Năm sinh của mẹ');
            $table->string('mobile_mother')->unique()->comment('Số điện thoại di động của me');
            $table->string('work_unit_mother')->default('')->comment('Đơn vị công tác của mẹ');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
