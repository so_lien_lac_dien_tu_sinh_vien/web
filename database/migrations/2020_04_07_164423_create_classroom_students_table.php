<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_students', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_class')->default('')->comment('Lớp học');

            $table->string('code_number_student')->default('')->comment('Sinh viên');

            $table->foreign('code_number_student')
                ->references('code_number_student')->on('students')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_class')
                ->references('code_number_class')->on('classrooms')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_students');
    }
}
