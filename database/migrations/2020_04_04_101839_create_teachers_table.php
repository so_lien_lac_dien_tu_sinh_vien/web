<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();

            // Thông tin cá nhân
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');

            $table->string('code_number_teacher')->unique()->comment('Mã giảng viên');
            $table->string('last_name')->comment('Họ');
            $table->string('first_name')->comment('Tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->date('birthday')->comment('Ngày sinh');
            $table->string('thumbnails')->default('')->comment('Hình đại diện, đường dẫn trên CDN, không kèm tên miền');

            $table->string('email')->unique()->comment('Địa chỉ email');
            $table->timestamp('email_verified_at')->nullable()->comment('Email xác minh thành công khi nào');
            $table->string('identity_number')->default('')->comment('CMND/Thẻ căn cước');

            $table->string('phone')->default('')->comment('Số điện thoại cố định');
            $table->string('mobile')->unique()->comment('Số điện thoại di động');
            $table->timestamp('mobile_verified_at')->nullable()->comment('Số điện thoại xác minh thành công khi nào');

            // Thông tin bổ sung, bổ trợ tìm kiếm

            $table->string('place_of_birth')->default('')->comment('Địa chỉ nơi sinh');
            $table->string('home_town')->default('')->comment('Quê quán');
            $table->string('permanent_residence')->default('')->comment('Hộ khẩu thường trú');
            $table->string('current_home')->default('')->comment('Nơi ở hiện tại');

            $table->string('nation')->default('')->comment('Dân tộc');
            $table->string('religion')->default('')->comment('Tôn giáo');
            $table->string('nationality')->default('')->comment('Quốc tịch');

            $table->string('level')->default('')->comment('Trình độ');
            $table->string('specialized')->default('')->comment('Chuyên ngành');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
