<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeroomTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeroom_teachers', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_teacher')->default('')->comment('Giáo viên chủ nhiệm');
            $table->string('code_number_class')->default('')->comment('Lớp học phụ trách');
            $table->unsignedBigInteger('code_number_school_year')->comment('Năm học phụ trách');
            $table->string('code_number_subject')->default('')->comment('Môn học phụ trách');

            $table->foreign('code_number_teacher')
                ->references('code_number_teacher')->on('teachers')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_school_year')
                ->references('id')->on('school_years')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_subject')
                ->references('code_number_subject')->on('subjects')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homeroom_teachers');
    }
}
