<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_department')->default('')->comment('Lớp học');
            $table->string('code_number_class')->default('')->comment('Lớp học');
            $table->unsignedBigInteger('code_number_school_year')->nullable()->comment('Năm học');
            $table->string('code_number_subject')->default('')->comment('Môn học');
            $table->string('code_number_student')->default('')->comment('Sinh viên');
            $table->string('code_number_teacher')->default('')->comment('Giảng viên');

            $table->foreign('code_number_department')
                ->references('code_number_department')->on('departments')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_school_year')
                ->references('id')->on('school_years')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_student')
                ->references('code_number_student')->on('students')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_teacher')
                ->references('code_number_teacher')->on('teachers')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_subject')
                ->references('code_number_subject')->on('subjects')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
