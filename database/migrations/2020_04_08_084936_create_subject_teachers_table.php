<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_teachers', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_subject')->default('')->comment('Môn học');

            $table->string('code_number_teacher')->default('')->comment('Giảng viên');

            $table->foreign('code_number_teacher')
                ->references('code_number_teacher')->on('teachers')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_subject')
                ->references('code_number_subject')->on('subjects')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_teachers');
    }
}
