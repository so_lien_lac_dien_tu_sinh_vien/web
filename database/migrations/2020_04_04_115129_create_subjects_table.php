<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_subject', 200)->unique()->comment('Mã môn học');
            $table->string('name', 200)->unique()->comment('Tên môn học');

            $table->string('code_number_department', 200)->comment('Mã phòng ban');
            $table->string('code_number_class', 200)->comment('Mã lớp học');
            $table->integer('count_credit')->comment('Số tín chỉ');

            $table->unsignedBigInteger('code_number_school_year')->comment('Năm học phụ trách');

            $table->string('code_number_teacher')->default('')->comment('Giảng viên');

            $table->index(['code_number_department', 'code_number_subject']);

            $table->foreign('code_number_school_year')
                ->references('id')->on('school_years')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_department')
                ->references('code_number_department')->on('departments')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_class')
                ->references('code_number_class')->on('classrooms')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_teacher')
                ->references('code_number_teacher')->on('teachers')
                ->onUpdate('restrict')
                ->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
