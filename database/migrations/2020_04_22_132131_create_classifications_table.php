<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifications', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_class')->default('')->comment('Lớp học');

            $table->unsignedBigInteger('code_number_school_year')->comment('Năm học');

            $table->string('code_number_student')->default('')->comment('Sinh viên');

            $table->unsignedBigInteger('classification')->comment('Xếp loại (1: Xuất sắc, 2: Giỏi, 3: Khá, 4: Trung bình, 5: Yếu)');

            $table->foreign('code_number_school_year')
                ->references('id')->on('school_years')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('code_number_student')
                ->references('code_number_student')->on('students')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifications');
    }
}
