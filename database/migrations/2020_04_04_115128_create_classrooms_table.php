<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_class', 200)->unique()->comment('Mã lớp học');
            $table->string('name', 200)->unique()->comment('Tên lớp học');

            $table->string('code_number_department', 200)->comment('Mã phòng ban');

            $table->index(['code_number_department', 'code_number_class']);

            $table->foreign('code_number_department')
                ->references('code_number_department')->on('departments')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
    }
}
