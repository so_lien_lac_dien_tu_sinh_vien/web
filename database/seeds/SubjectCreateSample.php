<?php

use App\Subject;
use Illuminate\Database\Seeder;

class SubjectCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/subject.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Subject ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $subject = Subject::create($item);
            $subject->save();
        }

        echo PHP_EOL;
    }
}
