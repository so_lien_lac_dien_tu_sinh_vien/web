<?php

use App\Student;
use Illuminate\Database\Seeder;

class StudentCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/student.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Student ${idx}/${c}";

            if (empty($item['code_number_student'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $student = Student::create($item);
            $student->save();
        }

        echo PHP_EOL;
    }
}
