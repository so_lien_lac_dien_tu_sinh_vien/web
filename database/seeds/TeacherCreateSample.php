<?php

use App\Teacher;
use Illuminate\Database\Seeder;

class TeacherCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/teacher.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Teacher ${idx}/${c}";

            if (empty($item['code_number_teacher'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $teacher = Teacher::create($item);
            $teacher->save();
        }

        echo PHP_EOL;
    }
}
