<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DepartmentCreateSample::class);
        $this->call(ClassroomCreateSample::class);

        $this->call(TeacherCreateSample::class);

        $this->call(SchoolYearCreateSample::class);
        $this->call(SubjectCreateSample::class);

        $this->call(StudentCreateSample::class);
        $this->call(UserSeeder::class);
    }
}
