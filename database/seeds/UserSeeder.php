<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            // Thông tin đăng nhập
            'username'              => 'demoad',
            'password'              => bcrypt('123456'),
            'code_number_teacher'   => 'GV001',
            'code_number_student'   => null,

            // Quyền hạn
            'role'                  => 'admin',
            'status'                => true,

        ]);

        User::create([
            // Thông tin đăng nhập
            'username'              => 'demosv',
            'password'              => bcrypt('123456'),
            'code_number_teacher'   => null,
            'code_number_student'   => 'SV001',

            // Quyền hạn
            'role'                  => 'student',
            'status'                => true,

        ]);

        User::create([
            // Thông tin đăng nhập
            'username'              => 'demogv',
            'password'              => bcrypt('123456'),
            'code_number_teacher'   => 'GV001',
            'code_number_student'   => null,

            // Quyền hạn
            'role'                  => 'teacher',
            'status'                => true,
        ]);
    }
}
