<?php

use App\SchoolYear;
use Illuminate\Database\Seeder;

class SchoolYearCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/school_year.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing SchoolYear ${idx}/${c}";

            if (empty($item['name_year'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $schoolYear = SchoolYear::create($item);
            $schoolYear->save();
        }

        echo PHP_EOL;
    }
}
