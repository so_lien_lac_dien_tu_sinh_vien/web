import AttendanceService from "../../services/attendance";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    code_number_class: "",
    code_number_school_year: "",
    code_number_subject: "",
    code_number_student: "",
    code_number_teacher: "",
    date_absent: "",
    absent: 0,
    reason_for_absence: ""
  }
};

const AttendanceAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        code_number_class: state.basicInfo.code_number_class,
        code_number_school_year: state.basicInfo.code_number_school_year,
        code_number_subject: state.basicInfo.code_number_subject,
        code_number_student: state.basicInfo.code_number_student,
        code_number_teacher: state.basicInfo.code_number_teacher,
        date_absent: state.basicInfo.date_absent,
        absent: state.basicInfo.absent,
        reason_for_absence: state.basicInfo.reason_for_absence
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        AttendanceService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.code_number_class = basicInfo.code_number_class;
      state.basicInfo.code_number_school_year =
        basicInfo.code_number_school_year;
      state.basicInfo.code_number_subject = basicInfo.code_number_subject;
      state.basicInfo.code_number_student = basicInfo.code_number_student;
      state.basicInfo.code_number_teacher = basicInfo.code_number_teacher;
      state.basicInfo.date_absent = basicInfo.date_absent;
      state.basicInfo.absent = basicInfo.absent;
      state.basicInfo.reason_for_absence = basicInfo.reason_for_absence;
    },

    reset(state) {
      state.basicInfo.code_number_class = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default AttendanceAddModule;
