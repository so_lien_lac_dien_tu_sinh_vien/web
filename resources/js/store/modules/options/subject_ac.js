import SubjectAcService from "../../../services/subject";

const SubjectAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    subjectOptions: []
  },
  actions: {
    loadSubjectData({ commit }) {
      commit("onLoadSubjectData");
      return new Promise((resolve, reject) => {
        SubjectAcService.getAll()
          .then(resp => {
            console.log(resp);
            commit("onProcessingSubjectData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSubjectDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadSubjectData(state) {
      state.isLoading = true;
    },
    onProcessingSubjectData(state, data) {
      state.isLoading = false;

      state.subjectOptions = [{ value: 0, text: "--- Chọn Môn học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.subjectOptions.push({
          value: data[i]["code_number_subject"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingSubjectDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default SubjectAcModule;
