import StudentAcService from "../../../services/student";

const StudentAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    studentOptions: []
  },
  actions: {
    loadStudentData({ commit }) {
      commit("onLoadStudentData");
      return new Promise((resolve, reject) => {
        StudentAcService.getAll()
          .then(resp => {
            console.log(resp);
            commit("onProcessingStudentData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingStudentDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadStudentData(state) {
      state.isLoading = true;
    },
    onProcessingStudentData(state, data) {
      state.isLoading = false;

      state.studentOptions = [{ value: 0, text: "--- Chọn Sinh viên ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.studentOptions.push({
          value: data[i]["code_number_student"],
          text: data[i]["last_name"] + " " + data[i]["first_name"]
        });
      }
    },
    onProcessingStudentDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default StudentAcModule;
