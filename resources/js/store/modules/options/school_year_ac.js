import SchoolYearAcService from "../../../services/school_year";
// import AcService from "../../../services/department_ac";

const SchoolYearAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    schoolYearOptions: [],
    // subjectOptions: []
  },
  actions: {
    loadSchoolYearData({ commit }) {
      commit("onLoadSchoolYearData");
      return new Promise((resolve, reject) => {
        SchoolYearAcService.getAll()
          .then(resp => {
            console.log(resp);
            commit("onProcessingSchoolYearData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSchoolYearDataFailure");
            reject(err);
          });
      });
    },

    // changeDepartmentSchoolYearToSubject(
    //   { commit },
    //   departmentId,
    //   schoolYearId
    // ) {
    //   return new Promise((resolve, reject) => {
    //     AcService.getSubjectBundle(departmentId, schoolYearId)
    //       .then(resp => {
    //         commit("onProcessingDepartmentSchoolYearData", resp.data.data);
    //
    //         resolve(resp);
    //       })
    //       .catch(err => {
    //         reject(err);
    //       });
    //   });
    // },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadSchoolYearData(state) {
      state.isLoading = true;
    },
    onProcessingSchoolYearData(state, data) {
      state.isLoading = false;

      state.schoolYearOptions = [{ value: 0, text: "--- Chọn Năm học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.schoolYearOptions.push({
          value: data[i]["id"],
          text: data[i]["name_year"] + "-" + data[i]["name_semester"]
        });
      }
    },
    onProcessingSchoolYearDataFailure(state) {
      state.isLoading = false;
    }

    // onProcessingDepartmentSchoolYearData(state, data) {
    //   state.districts = data;
    //
    //   state.subjectOptions = [{ value: 0, text: "--- Chọn môn học ---" }];
    //
    //   for (let i = 0, l = data.length; i < l; i++) {
    //     state.subjectOptions.push({
    //       value: data[i]["code_number_subject"],
    //       text: data[i]["name"]
    //     });
    //   }
    // }
  }
};

export default SchoolYearAcModule;
