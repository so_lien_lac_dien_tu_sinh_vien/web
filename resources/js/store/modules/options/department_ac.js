import AcService from "../../../services/department_ac";
import * as _ from "lodash";

const CategoryModule = {
  namespaced: true,
  state: {
    isLoading: false,

    departmentOptions: [],
    classroomOptions: [],
    subjectOptions: [],
    studentOptions: [],
    teacherOptions: [],
    subjectByDepartmentSchoolYearOptions: []
  },
  actions: {
    loadDepartmentData({ commit }) {
      commit("onLoadDepartmentData");
      return new Promise((resolve, reject) => {
        AcService.getDepartments()
          .then(resp => {
            // console.log(resp);
            commit("onProcessingDepartmentData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingDepartmentDataFailure");
            reject(err);
          });
      });
    },

    changeDepartment({ commit }, departmentId) {
      return new Promise((resolve, reject) => {
        localStorage.removeItem("departmentId");
        localStorage.setItem("departmentId", departmentId);
        AcService.getClassrooms(departmentId)
          .then(resp => {
            commit("onProcessingClassroomData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeDepartmentToSubject({ commit }, departmentId) {
      return new Promise((resolve, reject) => {
        AcService.getSubjects(departmentId)
          .then(resp => {
            // console.log(resp.data.data);
            commit("onProcessingSubjectData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeDepartmentSchoolYearToSubject(
      { commit },
      // departmentId,
      schoolYearId
    ) {
      var departmentId = localStorage.departmentId;
      return new Promise((resolve, reject) => {
        AcService.getSubjectBundle(departmentId, schoolYearId)
          .then(resp => {
            commit("onProcessingDepartmentSchoolYearData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeSubjectToTeacher({ commit }, subjectId) {
      return new Promise((resolve, reject) => {
        AcService.getTeacher(subjectId)
          .then(resp => {
            // console.log(resp.data.data);
            commit("onProcessingTeacherData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeClassroom({ commit }, classroom_id) {
      return new Promise((resolve, reject) => {
        AcService.getClassroomBundle(classroom_id)
          .then(resp => {
            commit("onProcessingClassroomBundleData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadDepartmentData(state) {
      state.isLoading = true;
    },
    onProcessingDepartmentData(state, data) {
      state.isLoading = false;

      state.departmentOptions = [{ value: 0, text: "--- Chọn Phòng ban ---" }];
      state.classroomOptions = [{ value: 0, text: "--- Chọn lớp học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.departmentOptions.push({
          value: data[i]["code_number_department"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingDepartmentDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingClassroomData(state, data) {
      state.districts = data;

      state.classroomOptions = [{ value: 0, text: "--- Chọn lớp học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.classroomOptions.push({
          value: data[i]["code_number_class"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingSubjectData(state, data) {
      state.districts = data;

      state.subjectOptions = [{ value: 0, text: "--- Chọn môn học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.subjectOptions.push({
          value: data[i]["code_number_subject"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingTeacherData(state, data) {
      state.districts = data;

      state.subjectOptions = [{ value: 0, text: "--- Chọn môn học ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.teacherOptions.push({
          value: data[i]["code_number_teacher"],
          text: data[i]["code_number_teacher"]
        });
      }
    },

    onProcessingClassroomBundleData(state, data) {
      state.studentOptions = [{ value: 0, text: "--- Chọn sinh viên ---" }];

      if (data.students) {
        let students = _.orderBy(data.students, ["last_name"], ["first_name"]);
        for (let i = 0, l = students.length; i < l; i++) {
          state.studentOptions.push({
            value: students[i]["id"],
            text: students[i]["last_name"] + " " + students[i]["first_name"]
          });
        }
      }
    },

    onProcessingDepartmentSchoolYearData(state, data) {
      state.districts = data;

      state.subjectByDepartmentSchoolYearOptions = [
        { value: 0, text: "--- Chọn môn học ---" }
      ];

      for (let i = 0, l = data.length; i < l; i++) {
        state.subjectByDepartmentSchoolYearOptions.push({
          value: data[i]["code_number_subject"],
          text: data[i]["name"]
        });
      }
    }
  }
};

export default CategoryModule;
