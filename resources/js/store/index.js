import Vue from "vue";
import Vuex from "vuex";

import ScorecardAddModule from "../store/modules/scorecard_add";
import AttendanceAddModule from "../store/modules/attendance_add";

// OPTIONS
import DepartmentAcModule from "../store/modules/options/department_ac";
import SchoolYearAcModule from "../store/modules/options/school_year_ac";
import StudentAcModule from "../store/modules/options/student_ac";
import SubjectAcModule from "../store/modules/options/subject_ac";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        scorecard_add: ScorecardAddModule,
        attendance_add: AttendanceAddModule,

        // OPTIONS
        department_ac: DepartmentAcModule,
        school_year_ac: SchoolYearAcModule,
        student_ac: StudentAcModule,
        subject_ac: SubjectAcModule,
    }
});