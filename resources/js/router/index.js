import Router from "vue-router";

// overview
import Overview from "../views/overview/index";

// // scorecard
import Scorecard from "../views/scorecard/index";
import ScorecardEdit from "../views/scorecard/edit";
import ScorecardAdd from "../views/scorecard/add";
//
// // Attendance
import Attendance from "../views/attendance/index";
import AttendanceEdit from "../views/attendance/edit";
import AttendanceAdd from "../views/attendance/add";

import Vue from "vue";
Vue.use(Router);

// Routes
const router = new Router({
    linkExactActiveClass: "active", // link active
    mode: "hash",
    base: process.env.BASE_URL,
    routes: [
        { path: '/', name: 'overview.index', component: Overview },
        // { path: '/attendance', name: 'attendance.index', component: Attendance }
        { path: '/scorecard', name: 'scorecard.index', component: Scorecard },
        { path: '/scorecard/edit/:id', name: 'scorecard.edit', component: ScorecardEdit },
        { path: '/scorecard/add', name: 'scorecard.add', component: ScorecardAdd },

        { path: '/attendance', name: 'attendance.index', component: Attendance },
        { path: '/attendance/edit/:id', name: 'attendance.edit', component: AttendanceEdit },
        { path: '/attendance/add', name: 'attendance.add', component: AttendanceAdd }
    ]
});

export default router;