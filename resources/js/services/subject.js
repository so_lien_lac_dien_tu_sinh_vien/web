import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("api/subject/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/subject/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/subject/create", data);
}

function show(id) {
  return axios.get("api/subject/show/" + id);
}

function update(id, data) {
  return axios.post("api/subject/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/subject/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/subject/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch
};
