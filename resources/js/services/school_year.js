import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("api/schoolYear/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/schoolYear/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/schoolYear/create", data);
}

function show(id) {
  return axios.get("api/schoolYear/show/" + id);
}

function update(id, data) {
  return axios.post("api/schoolYear/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/schoolYear/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/schoolYear/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch
};
