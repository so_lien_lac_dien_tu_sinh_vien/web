import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("api/student/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/student/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/student/create", data);
}

function show(id) {
  return axios.get("api/student/show/" + id);
}

function update(id, data) {
  return axios.post("api/student/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/student/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/student/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("api/student/upload", formData, config);
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch,
  upload
};
