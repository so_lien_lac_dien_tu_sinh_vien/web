import axios from "axios";

function getDepartments() {
  return axios.get("api/department/list");
}

function getDepartment(id) {
  return axios.get("api/department/info/" + id);
}

function getClassrooms(id) {
  return axios.get("api/department/getClassrooms/" + id);
}

function getSubjects(id) {
  return axios.get("api/department/getSubjects/" + id);
}

function getClassroomBundle(id) {
  return axios.get("api/getClassroom/bundle/" + id);
}

function getSubjectBundle(departmentId, schoolYearId) {
  return axios.get(
    "api/department/getSubjects/bundle/" + departmentId + "/" + schoolYearId
  );
}

function getTeacher(id) {
  return axios.get("api/department/getTeacher/" + id);
}

export default {
  getDepartments,
  getDepartment,
  getClassrooms,
  getSubjects,
  getClassroomBundle,
  getSubjectBundle,

  getTeacher
};
