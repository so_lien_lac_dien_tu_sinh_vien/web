require('./bootstrap');

import Axios from "axios";
import CKEditor from "@ckeditor/ckeditor5-vue";
Vue.use(CKEditor);

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Axios.defaults.baseURL = "http://app.airlock-example.test:8000/"; // Đường dẫn đầu tiên + service api

new Vue({
    el: '#root',
    template: `<app></app>`,
    components: { App },
    router,
    store
}).$mount("#app");