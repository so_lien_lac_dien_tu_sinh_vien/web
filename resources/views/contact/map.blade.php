@extends('layouts.basic')
@section('content')
    <section class="contact-section">
        <div class="container">
            <div class="row">
                <!--Google map-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6524.087993089738!2d106.672488630414!3d10.980559266718492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d12739bb468f%3A0xe2bf4f397d0aa76!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBUaOG7pyBE4bqndSBN4buZdA!5e0!3m2!1svi!2s!4v1584085921396!5m2!1svi!2s" width="100%" height="90%" frameborder="0" style="border:0; min-height: 500px" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <!--Google Maps-->
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title">Lấy thông tin</h2>
                </div>
                <div class="col-lg-8">
                    @if(Session::has('customer'))
                        <div class="alert alert-{{Session::get('customer')}} text-center" role="alert">
                            <b>{{Session::get('customer-notification')}}</b>
                        </div>
                    @endif
                    <form class="form-contact contact_form" action="{{route('contact.post')}}" method="post"
                          novalidate="novalidate">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control w-100" name="description" id="description" cols="30" rows="9"
                                              placeholder="Nội dung của bạn..."></textarea>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input class="form-control valid" name="full_name" id="full_name" type="text"
                                           placeholder="Họ tên của bạn...">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select name="sex" class="form-control form-control-lg">
                                        <option value="1">Nam</option>
                                        <option value="0">Nữ</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input class="form-control valid" name="email" id="email" type="email" placeholder="Địa chỉ Email...">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="mobile" id="mobile" type="text"
                                           placeholder="Số điện thoại...">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Gửi</button>
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body">
                            <h3>Edumark</h3>
                            <p>Thủ Dầu Một, Bình Dương</p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body">
                            <h3>+(84) 937 152 3690</h3>
                            <p>24/24</p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            <h3>edumark@contact.com</h3>
                            <p>Gửi cho chúng tôi yêu cầu của bạn bất cứ lúc nào!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection