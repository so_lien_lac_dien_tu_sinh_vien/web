<div class="container" id="checkpoint">
    @foreach($result as $scorecard)
        <b style="margin-top: 50px">{{$scorecard['scorecard'][0]['schoolYears']['name_year']}}
            - {{$scorecard['scorecard'][0]['schoolYears']['name_semester']}}</b>
        <table class="table">
            <thead>
            <tr>
                <!-- <th scope="col">STT</th> -->
                <th scope="col">Môn học</th>
                <th scope="col">Số tín chỉ</th>
                <th scope="col">Điểm giữa kì</th>
                <th scope="col">Điểm cuối kì</th>
                <th scope="col">Trung bình môn</th>
            </tr>
            </thead>
            <tbody>
            @foreach($scorecard['scorecard'] as $item)
                <tr>
                    <!-- <th scope="row">{{ $loop->index + 1 }}</th> -->
                    <td width="150px">{{$item->subjects['name']}}</td>
                    <td>{{$item->subjects['count_credit']}}</td>
                    <td>{{$item->mid_score}}</td>
                    <td>{{$item->last_score}}</td>
                    <td>{{$item->medium_score}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row mb-3">
            <p>Điểm trung bình học kì: <span
                        class="text-danger">{{number_format($scorecard['scorecard']->avg('medium_score'), 2)}}</span>
            </p>
            <p class="ml-5">Nghỉ có phép: {{$scorecard['attendanceYes']}}</p>
            <p class="ml-5">Nghỉ không phép: {{$scorecard['attendanceNo']}}</p>
            @if($scorecard['classification'][0]['classification'] == 1)
                <p class="ml-5">Xếp loại: Giỏi</p>
            @elseif($scorecard['classification'][0]['classification'] == 2)
                <p class="ml-5">Xếp loại: Khá</p>
            @elseif($scorecard['classification'][0]['classification'] == 3)
                <p class="ml-5">Xếp loại: Trung bình</p>
            @elseif($scorecard['classification'][0]['classification'] == 4)
                <p class="ml-5">Xếp loại: Yếu</p>
            @endif
            <!-- <p class="ml-5">Điểm rèn luyện: {{$scorecard['attendanceNo']}}</p> -->
            <p class="ml-5">Điểm rèn luyện: {{$scorecard['trainingPoint'][0]['training_point']}}</p>

        </div>
    @endforeach
</div>

{{--<div class="container" id="eachPoint">--}}
{{--    <table class="table">--}}
{{--        <thead>--}}
{{--        <tr>--}}
{{--            <!-- <th scope="col">STT</th> -->--}}
{{--            <th scope="col">Môn học</th>--}}
{{--            <th scope="col">Số tín chỉ</th>--}}
{{--            <th scope="col">Điểm giữa kì</th>--}}
{{--            <th scope="col">Điểm cuối kì</th>--}}
{{--            <th scope="col">Trung bình môn</th>--}}
{{--        </tr>--}}
{{--        </thead>--}}
{{--        <tbody>--}}

{{--        </tbody>--}}
{{--    </table>--}}
{{--</div>--}}
