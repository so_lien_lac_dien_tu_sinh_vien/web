@extends('layouts.basic')
@section('content')
    <!-- popular_courses_start -->
    <div class="popular_courses">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="course_nav">
                        <nav>
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">Điểm cập nhật</a>
                                </li>
                                <li>
                                    <a>{{$result[0]['scorecard'][0]['students']['last_name']}} {{$result[0]['scorecard'][0]['students']['first_name']}}</a>
                                    <br>
                                    <a>{{$result[0]['scorecard'][0]['classrooms']['name']}}</a>

                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
        <div class="all_courses">
            <div class="container">
                <div class="tab-content" id="myTabContent">
                    <div style="margin-left: 450px">
                        <form class="form-inline my-2 my-lg-0" action="{{route('searchSemester')}}" method="GET">
                            <select id="semester" name="semester" class="form-control">
                                <option>-Chọn học kì-</option>

                                @foreach($SchoolYearOptions as $item)
                                    <option
                                        value="{{$item['id'][0]['id']}}">{{$item['id'][0]['name_year']}}
                                        - {{$item['id'][0]['name_semester']}}
                                    </option>
                                @endforeach
                                <option value="null">Tất cả học kì</option>
                                <!-- <option value="null">Tất cả học kì</option> -->
                            </select>
{{--                            <button class="btn btn-light" type="submit">Tìm kiếm</button>--}}
                        </form>

                    </div>

                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        @include('scorecard.common.scorecard')
                    </div>
                    @if($sumDiemTichLuy)
                        <p>Điểm trung bình tích lũy: {{$sumDiemTichLuy}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- popular_courses_end-->
@endsection

@section('js_footer')
    @parent
    <script>
        $(function(){
            $('#semester').change(function ()  {
                $value = $(this).val();
                if ($value != "null") {
                    window.location.href = "http://127.0.0.1:8085/searchSemester?semester=" + $value;
                } else if($value == "null") {
                    window.location.href = "http://127.0.0.1:8085/tim-kiem-diem-sv";
                } else{
                    console.log('chọn tầm bậy nha!');
                }
            });
        });

    </script>
@endsection
