@extends('login.index')
@section('content')
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Edumark</h3>
                    <div class="d-flex justify-content-end social_icon">
                        <span><i class="fab fa-facebook-square"></i></span>
                        <span><i class="fab fa-google-plus-square"></i></span>
                        <span><i class="fab fa-twitter-square"></i></span>
                    </div>
                </div>
                <div class="card-body">
                    @if(Session::has('flag'))
                        <div class="alert alert-{{Session::get('flag')}} text-center" role="alert">
                            <b>{{Session::get('login-notification')}}</b>
                        </div>
                    @endif
                    @if(Session::has('changePass'))
                        <div class="alert alert-{{Session::get('changePass')}} text-center" role="alert">
                            <b>{{Session::get('password-notification')}}</b>
                        </div>
                    @endif
                    <form action="{{route('login.post')}}" method="POST">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="username" class="form-control" placeholder="Tài khoản...">

                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu...">
                        </div>
                        <div class="row align-items-center remember">
                            <input type="checkbox">Lưu tài khoản
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Đăng nhập" class="btn float-right login_btn">
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        <a class="text-white" href="{{route('get.password.reset')}}">Quên mật khẩu?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection