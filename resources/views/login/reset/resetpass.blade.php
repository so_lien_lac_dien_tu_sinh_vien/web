@extends('login.index')
@section('content')
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Edumark</h3>
                    <div class="d-flex justify-content-end social_icon">
                        <span><i class="fab fa-facebook-square"></i></span>
                        <span><i class="fab fa-google-plus-square"></i></span>
                        <span><i class="fab fa-twitter-square"></i></span>
                    </div>
                </div>
                <div class="card-body">
                    @if(Session::has('changePass'))
                        <div class="alert alert-{{Session::get('changePass')}} text-center" role="alert">
                            <b>{{Session::get('password-notification')}}</b>
                        </div>
                    @endif
                    <form method="POST">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="password" name="password_reset" class="form-control" placeholder="Mật khẩu mới...">
                            <input type="password" name="re_password_reset" class="form-control" placeholder="Xác nhận lại mật khẩu...">

                        </div>
                        <div class="form-group">
                            <input type="submit" value="Xác nhận" class="btn float-right login_btn w-100">
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        <a class="text-white" href="{{route('login.get')}}"> Đã có tài khoản?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection