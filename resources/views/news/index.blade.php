@extends('layouts.basic')
@section('content')
    <!-- popular_courses_start -->
    <div class="popular_courses">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="course_nav">
                        <nav>
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">Tin cập nhật</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
        <div class="all_courses">
            <div class="container">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            @foreach($news as $item)
                                <div class="col-xl-4 col-lg-4 col-md-6">
                                    <div class="single_courses">
                                        <div class="thumb">
                                            <a href="{{route('news.detail', ['slug' => $item->slug, 'id' => $item->id])}}" class="" style="text-decoration: none">
                                                <img src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}" alt="">
                                            </a>
                                        </div>
                                        <div class="courses_info">
                                            <span>{{$item->getNewsCategoryString}}</span>
                                            <h3><a href="#">{{$item->title}}<br></a></h3>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular_courses_end-->
@endsection