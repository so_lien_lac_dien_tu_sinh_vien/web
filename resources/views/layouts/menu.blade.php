<header>
    <div class="header-area" style="background-color: #6f42c1">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <a href="/">
                                <img src="/img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="main-menu  d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
{{--                                    <li><a class="" href="{{route('scorecard.index')}}">Xem điểm</a></li>--}}
{{--                                    <li><a href="Courses.html">Xem điểm danh</a></li>--}}
                             <!--        @if(Auth::check())
                                    <li><a class="{{ Route::currentRouteName() == 'post.form' ? 'active' : '' }}" href="{{route('post.form')}}">Sinh viên</a></li>
                                    @endif -->
                                    <li><a class="{{ Route::currentRouteName() == 'contact.index' ? 'active' : '' }}" href="{{route('contact.index')}}">Ý kiến phản hồi</a></li>
                                    <li><a class="{{ Route::currentRouteName() == 'news.index' ? 'active' : '' }}" href="{{route('news.index')}}">Tin tức</a></li>
{{--                                    <li><a href="#search-form" class="popup-with-form">Tra cứu điểm</a></li>--}}
                                    <li>
                                        <form action="{{route('search')}}" method="GET">
                                            <div class="row">
                                                <div class="col-xl-10 col-md-10">
                                                    <input class="form-control" type="search" id="query" name="query"
                                                           value="{{request()->input('query')}}" placeholder="Nhập mã số sinh viên cần tra cứu..."/>
                                                </div>
                                                <div class="col-xl-1">
                                                    <button type="submit" class="btn btn-dark">Tìm kiếm</button>
                                                </div>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                        <div class="log_chat_area d-flex align-items-center">
                            @if(Auth::check())
                                <a href="#"
                                   class="login">
                                    <i class="flaticon-user"></i>
                                    {{Auth::user()->username}}
                                </a>
                                <a href="{{route('logout')}}" class="login">
                                    <i class="fa fa-sign-out"></i>
                                    Đăng xuất
                                </a>
                            @else
                                <a href="{{route('login.get')}}" class="login">
                                    <i class="fa fa-sign-in"></i>
                                    <span>Đăng nhập</span>
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mb-5"></div>

<!-- <div class="slider_area ">
    <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-6 col-md-6">
                    <div class="illastrator_png">
                        <img src="/img/banner/edu_ilastration.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="slider_info">
                        <h3>Sổ liên lạc <br>
                            điện tử <br>
                            </h3>
                        <a href="#" class="boxed_btn">Edumark</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->