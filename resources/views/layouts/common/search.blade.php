<form id="search-form" class="white-popup-block mfp-hide">
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <form action="{{route('search')}}" method="GET">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="search" id="query" name="query"
                               value="{{request()->input('query')}}" placeholder="Nhập mã số sinh viên cần tra cứu..."/>
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" class="boxed_btn_orange">Tìm kiếm</button>
                    </div>
                </div>
            </form>
            </p>
        </div>
    </div>
</form>