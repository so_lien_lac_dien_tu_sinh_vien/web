<footer class="footer footer_bg_1">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="#">
                                <img src="img/logo.png" alt="">
                            </a>
                        </div>
                        <p>
                            Là hệ thống bảng điểm và xếp loại trực tuyến nhằm mục đích cho các sinh viên và phụ huynh dễ dàng theo dõi một cách trực quan về bảng điểm và chuyên cần của sinh viên.
                        </p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Edumark
                        </h3>
                        <ul>
                            <li><a href="{{route('contact.index')}}">Về chúng tôi</a></li>
                            <li><a href="#"> Chính sách bảo mật</a></li>
                            <li><a href="#">Điều khoản hoạt động</a></li>
                            <li><a href="#">Tin tức</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Hỗ trợ
                        </h3>
                        <ul>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                            <li><a href="#">Hướng dẫn sử dụng</a></li>
                            <li><a href="#">Hotline 0937.191.013</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Kết nối
                        </h3>
                        <p>
                            Thủ Dầu Một, Bình Dương <br>
                            +10 367 467 8934 <br>
                            edumark@contact.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Bản quyền thuộc Ngô Bá Cường
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>